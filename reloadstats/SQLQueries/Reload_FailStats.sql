        SELECT DISTINCT RLO.userid                                                       -- This Select removes "retries"
       ,min(RLO.createddate) AS createddate                                              -- ie: duplicate failures  
       ,RLO.ReasonCode                                                                   -- for the same user same day
       ,min(RLO.first_reload_success_time) AS first_reload_success_time
       ,min(cast(RLO.duration AS int)) AS duration
FROM
(       SELECT  distinct  FO.UserId                                                  
              ,FO.createddate                                                 
              ,FO.reasoncode 
              ,OH.d_datecreated AS First_Reload_Success_Time
                       ,cast(Datediff(second, FO.createddate, OH.d_datecreated) AS INT) AS Duration                
       FROM failedorder FO LEFT JOIN OrderFormHeader OH ON FO.UserId = OH.user_id and OH.d_datecreated > FO.createddate
          WHERE FO.createddate > '01/01/2019' --and FO.Userid = '{00045CB3-B2D6-456D-AED5-7902885CE5DF}'
          ) RLO
GROUP BY RLO.UserId
              ,substring(cast(RLO.createddate as varchar), 1, 10)
              ,RLO.reasoncode
              order by RLO.userid
