               SELECT  distinct  OH1.User_id                                                  
              ,OH1.d_DateCreated AS First_Reload_Success_Time                                               
              ,OH2.d_datecreated AS Next_Reload_Success_Time
                       ,OH2.cc_total
                       ,cast(Datediff(second, OH1.d_DateCreated, OH2.d_datecreated) AS INT) AS Duration                
       FROM OrderFormHeader OH1 LEFT JOIN OrderFormHeader OH2 with (nolock) ON OH1.User_Id = OH2.user_id  and OH2.d_DateCreated > OH1.d_DateCreated WHERE OH1.d_DateCreated between 'mm/dd/yyyy' and 'mm/dd/yyyy'  order by OH1.User_Id,OH1.d_DateCreated,OH2.d_datecreated
